FROM alpine:3.11.3

RUN apk add --update \
    nginx \
    tzdata \
    ca-certificates \
    dcron \
    zip \
    redis \
    php7 \
    php7-fpm \
    php7-common \
    php7-curl \
    php7-mbstring \
    php7-sqlite3 \
    php7-pdo_mysql \
    php7-pdo_sqlite \
    php7-dom \
    php7-simplexml \
    php7-tokenizer \
    php7-zip \
    php7-redis \
    php7-gd \
    php7-json \
    php7-pcntl \
    php7-fileinfo \
    php7-xdebug \
    && rm -rf /var/cache/apk/*

WORKDIR /html

RUN sed -i 's/;error_log = log\/php7\/error.log/error_log = \/dev\/stderr/g' /etc/php7/php-fpm.conf && \
    sed -i 's/;daemonize = yes/daemonize = no/g' /etc/php7/php-fpm.conf && \
    sed -i 's/listen = 127.0.0.1:9000/listen = 9000/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/;request_terminate_timeout = 0/request_terminate_timeout = 900/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/;request_terminate_timeout_track_finished = no/request_terminate_timeout_track_finished = yes/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/;decorate_workers_output = no/decorate_workers_output = no/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/;catch_workers_output = yes/catch_workers_output = yes/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/user = nobody/user = nginx/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/group = nobody/group = nginx/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/pm.max_children = 5/pm.max_children = 100/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/pm.start_servers = 2/pm.start_servers = 5/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/pm.min_spare_servers = 1/pm.min_spare_servers = 5/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/pm.max_spare_servers = 3/pm.max_spare_servers = 25/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/user = www-data/user = nginx/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/group = www-data/group = nginx/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php7/php.ini && \
    sed -i 's/expose_php = On/expose_php = Off/g' /etc/php7/php.ini && \
    sed -i 's/;zend_extension=xdebug.so/zend_extension=xdebug.so/g' /etc/php7/conf.d/xdebug.ini && \
    echo "xdebug.remote_enable = 1" >> /etc/php7/conf.d/xdebug.ini && \
    echo "xdebug.remote_autostart = 1" >> /etc/php7/conf.d/xdebug.ini && \
    echo "xdebug.remote_connect_back = 1" >> /etc/php7/conf.d/xdebug.ini && \
    echo "xdebug.idekey=VSCODE" >> /etc/php7/conf.d/xdebug.ini && \
    echo "daemonize yes" >> /etc/redis.conf && \
    ln -s /dev/null /var/log/nginx/access.log && \
    ln -s /dev/stdout /var/log/nginx/error.log && \
    cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    echo "Europe/Berlin" > /etc/timezone && \
    (crontab -l ; echo "* * * * * php /html/artisan schedule:run >> /dev/null 2>&1") | crontab -

WORKDIR /html
EXPOSE 80

COPY . /html
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx-default.conf /etc/nginx/conf.d/default.conf

CMD if [ ! -f .env ]; then cp .env.example .env; php artisan key:generate; fi && \
    chown -R root:nginx storage bootstrap/cache && \
    chmod -R g+w storage bootstrap/cache && \
    crond -L /dev/stdout && \
    php-fpm7
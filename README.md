# Dummy Engine

Dummy Search Engine to be used by [MetaGer](https://metager.de) -- [GitLab](https://gitlab.metager.de/open-source/MetaGer)

Returns a json with test results.

## Parameters

These parameters are given via get. (e.g. localhost:8080/?count=20&skip=10)

**count**
>sets the amount of results to be delivered. default = 10, limit = 100

**skip**
>sets the amount of results to be skipped. effectively just increases the dummy's number

**min**
**max**
>sets the range for a sleep in milliseconds if either min or max is given correctly. for exact timer both have to be equal. limit = 10000

**url**
>replaces the first result in the array with a given url.
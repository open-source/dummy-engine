<?php 

namespace App\Models;

class Result{
    public $title;
    public $link;
    public $descr;  
    public function __construct($title, $link, $descr){
        $this->title = $title;
        $this->link = $link;
        $this->descr = $descr;
    }
}
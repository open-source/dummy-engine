<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Result;
use joshtronic\LoremIpsum;
use \Illuminate\Http\Request;

class Resultjson extends Controller{
    public function getDummy(Request $request){

        define("MAX_COUNT",100);
        define("MAX_SLEEP",10000);

        $results = []; # array for results
        $lipsum = new LoremIpsum(); # php-loremipsum used to fill description of dummy result -- https://github.com/joshtronic/php-loremipsum/
        $skip = 0; # amout of results to skip.
        $count = 10; # amount of results to create, 10 by default
        $min = 0; $max = 0; # time in milliseconds to sleep

        # changes amount of results if param is valid
        if(!empty($request->input('count')) && is_numeric($request->input('count')) && (int)$request->input('count') > 0 && (int)$request->input('count') < MAX_COUNT) {
            $count = (int)$request->input('count');
        }
        
        # increases result counter if param is valid
        if(!empty($request->input('skip')) && is_numeric($request->input('skip')) && (int)$request->input('skip') > 0 && (int)$request->input('skip') < PHP_INT_MAX - MAX_COUNT) {
            $skip = (int)$request->input('skip');
        }

        # fills results array with created dummies
        for($i = 1; $i < $count+1; $i++) {
            $results[$i+$skip] = new Result('dummy-result' . ($i+$skip), 'https://'. $lipsum->word() . ($i+$skip) .'.com', $lipsum->words(10));
        }

        # replaces first array element with entered url if param is valid
        if(!empty($request->input('url'))) {
            $url = $request->input('url');
            if(filter_var($url, FILTER_VALIDATE_URL)) {
                $results[0+$skip] = new Result('entered-url', $url, $lipsum->words(10));
            }
        }

        # sets range for sleep command if at least 1 sleep param is valid
        if($this->validSleep($request->input('min')) && $this->validSleep($request->input('max'))) {
            if($request->input('min') > $request->input('max')){
                $min = (int)$request->input('max');
                $max = (int)$request->input('min');
            }else{
                $min = (int)$request->input('min');
                $max = (int)$request->input('max'); 
            }
        }elseif(!$this->validSleep($request->input('min')) && $this->validSleep($request->input('max'))) {
            $max = (int)$request->input('min'); 
        }elseif($this->validSleep($request->input('min')) && !$this->validSleep($request->input('max'))) {
            $min = (int)$request->input('min');
            $max = MAX_SLEEP;
        }

        if($max !== 0 && $min <= $max){
            usleep(rand($min*1000, $max*1000));
        }

        return response()->json($results);
    }
    # makes handling the sleep timer less messy
    private function validSleep($sleep, int $maximum = 10000){
        if(!empty($sleep) && is_numeric($sleep) && (int)$sleep > 0 && (int)$sleep < $maximum) {
            return true;
        }else{
            return false;
        }
    }
}